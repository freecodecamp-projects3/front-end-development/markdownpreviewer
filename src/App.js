import React from "react";

marked.setOptions({
  gfm: true,
  breaks: true,
});
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      markdownInput: initMarkdown,
    };
    this.handleMarkdownInput = this.handleMarkdownInput.bind(this);
  }
  componentDidMount() {
    const preview = document.getElementById("preview");
    preview.innerHTML = marked(initMarkdown);
  }
  handleMarkdownInput(event) {
    let newInputValue = event.target.value;
    this.setState({
      markdownInput: newInputValue,
    });
    const preview = document.getElementById("preview");
    preview.innerHTML = marked(newInputValue);
  }

  render() {
    return (
      <div id="container">
        <textarea
          value={this.state.markdownInput}
          onChange={this.handleMarkdownInput}
          id="editor"
          cols="60"
          rows="10"
        ></textarea>
        <div id="preview"></div>
      </div>
    );
  }
}
const initMarkdown =
  "# big header! \n## smaller header! \n[and this is link](www.freecodecamp.org)  \nhere is inline code `<h1></h1>` \n\n and here code block \n\n```\nconst function = () => Math.random() \n``` \n\n**bold text** \n>block quote \n\n ![image](https://pngimg.com/uploads/github/small/github_PNG63.png)  \n* list item \n";
export default App;
